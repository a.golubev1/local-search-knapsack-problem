package com.company;

import java.io.PrintWriter;
import java.util.Random;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Collections;

public class Functions
{
    boolean checkIfThingActive (int currentTime, Entity thing)
    {
        return thing.getLandingTime() <= currentTime && currentTime < thing.getDepartureTime();
    }

    int findFinishTime (Entity[] things, int numOfObj)
    {
        int endTime = 0;
        for (int j = 0; j < numOfObj; j++)
        {
            if (things[j].getDepartureTime() > endTime)
                endTime = things[j].getDepartureTime();
        }
        return endTime;
    }

    double calculateEnergy (double fine, Container[] store, Entity[] things, Cell[] answer, int numOfObj, int numOfCont, double temp)
    {
        double energyFromCont = 0.0, energyFromThings = 0.0;
        double sumWeight;
        int endTime = 0;
        boolean limitsCheck = true;
        double sumOverLoad = 0;
        endTime = findFinishTime(things, numOfObj);
        for (int t = 0; t < endTime; t++)
        {
            for (int i = 0; i < numOfCont; i++)
            {
                sumWeight = 0.0;
                for (int j = 0; j < answer[i].getThingsInCell().size(); j++)
                {
                    if (checkIfThingActive(t, things[answer[i].getThingsInCell().get(j)]))
                        sumWeight += things[answer[i].getThingsInCell().get(j)].getWeight();
                }
                double relativeLoad = sumWeight / store[i].getSumCapacity();
                if (sumWeight > store[i].getSumCapacity())
                    limitsCheck = false;
                if (relativeLoad > 1.0)
                {
                    energyFromThings += 1;
                    sumOverLoad += relativeLoad;
                }
                else
                    energyFromThings += Math.sqrt(relativeLoad);
            }
        }
        for (int i = 0; i < numOfCont; i++)
        {
            if (answer[i].getThingsInCell().size() != 0)
                energyFromCont++;
        }
        if (limitsCheck)
            System.out.println("Correct");
        else
            System.out.println("Incorrect");
        return energyFromCont + energyFromThings + fine * sumOverLoad;
    }

    int findWhereThingIs (Cell[] answer, int numOfCont, int numObj)
    {
        for (int i = 0; i < numOfCont; i++)
        {
            for (int j = 0; j < answer[i].getThingsInCell().size(); j++)
            {
                if (numObj == answer[i].getIndexOfThing(j))
                    return i;
            }
        }
        return -1;
    }

    void generateStateCandidate (Cell[] answer, int numOfCont, Container[] store, Cell[] stateCandidate, int numOfObj, Entity[] things)
    {
        final Random random = new Random();
        int randObj = 0, randCont = 0;
        boolean endOfCheck = true;
        int curCont = 0;
        while (endOfCheck)
        {
            randObj = random.nextInt(numOfObj);
            randCont = random.nextInt(numOfCont);
            curCont = findWhereThingIs(answer, numOfCont, randObj);
            if (randCont != curCont)
                endOfCheck = false;
        }
        int indexOfThing = 0;
        for (int i = 0; i < answer[curCont].getThingsInCell().size(); i++)
        {
            if (randObj == answer[curCont].getIndexOfThing(i))
                indexOfThing = i;
        }
        for (int i = 0; i < numOfCont; i++)
            stateCandidate[i].getThingsInCell().addAll(answer[i].getThingsInCell());
        stateCandidate[curCont].getThingsInCell().remove(indexOfThing);
        stateCandidate[randCont].getThingsInCell().add(randObj);
    }

    double getTransitionProbability (double dE, double t)
    {
        return Math.exp(-dE/t);
    }

    void eqAnswer (int numOfCont, Cell[] answer, Cell[] stateCandidate)
    {
        for (int i = 0; i < numOfCont; i++)
            answer[i].getThingsInCell().clear();
        for (int i = 0; i < numOfCont; i++)
        {
            for (int j = 0; j < stateCandidate[i].getThingsInCell().size(); j++)
                answer[i].getThingsInCell().add(stateCandidate[i].getThingsInCell().get(j));
        }
    }

    boolean makeTransit (double p)
    {
        final Random random = new Random();
        double value = random.nextDouble();
        return value <= p;
    }

    double decreaseTemperature(double initialTemperature, int i)
    {
        return initialTemperature *  0.1 / i;
    }

    void printForPlot (PrintWriter output, int countForPlot, double currentEnergy)
    {
        output.print(countForPlot);
        output.print(" ");
        output.print(currentEnergy);
        output.print("\n");
    }

    boolean checkForCorrect (Entity[] things, int numOfObj, int numOfCont, Cell[] answer, Container[] store)
    {
        double sumWeight;
        int endTime = findFinishTime(things, numOfObj);
        for (int t = 0; t < endTime; t++)
        {
            for (int i = 0; i < numOfCont; i++)
            {
                sumWeight = 0.0;
                for (int j = 0; j < answer[i].getThingsInCell().size(); j++)
                {
                    if (checkIfThingActive(t, things[answer[i].getThingsInCell().get(j)]))
                        sumWeight += things[answer[i].getThingsInCell().get(j)].getWeight();
                }
                if (sumWeight > store[i].getSumCapacity())
                    return false;
            }
        }
        return true;
    }

    void funcToAnswer (int numberOfThings, int capacity, boolean[] acceptedThing, ArrayList<Integer> thingsInCont, Entity[] things, double[][] func)
    {
        if (func[numberOfThings][capacity] == 0)
        {
            return;
        }
        else if (func[numberOfThings - 1][capacity] == func[numberOfThings][capacity])
            funcToAnswer(numberOfThings - 1, capacity, acceptedThing, thingsInCont, things, func);
        else
        {
            funcToAnswer(numberOfThings - 1, capacity - things[thingsInCont.get(numberOfThings - 1)].getWeight(), acceptedThing, thingsInCont, things, func);
            acceptedThing[numberOfThings - 1] = true;
        }
    }

    void knapsack (ArrayList<Integer> thingsInCont, Entity[] things, int[][] answerWithDiv, int capacity)
    {
        double[][] func = new double[thingsInCont.size() + 1][capacity + 1];
        boolean[] acceptedThing = new boolean[thingsInCont.size()];
        for (int i = 0; i < thingsInCont.size(); ++i)
            acceptedThing[i] = false;
        for (int i = 0; i <= capacity; ++i)
            func[0][i] = 0;
        for (int i = 1; i <= thingsInCont.size(); ++i)
        {
            for (int j = 0; j <= capacity; ++j)
            {
                func[i][j] = func[i - 1][j];
                if (j >= things[thingsInCont.get(i - 1)].getWeight() && (func[i - 1][j - things[thingsInCont.get(i - 1)].getWeight()] + things[thingsInCont.get(i - 1)].getWeight() > func[i][j]))
                    func[i][j] = func[i - 1][j - things[thingsInCont.get(i - 1)].getWeight()] + things[thingsInCont.get(i - 1)].getWeight();
            }
        }
        funcToAnswer(thingsInCont.size(), capacity, acceptedThing, thingsInCont, things, func);
        ArrayList<Integer> listForDelete = new ArrayList<Integer>();
        for (int i = 0; i < thingsInCont.size(); i++)
        {
            if (acceptedThing[i])
            {
                answerWithDiv[0][thingsInCont.get(i)] = 1;
                listForDelete.add(i);
            }
        }
        for (int i = listForDelete.size() - 1; i >= 0; i--)
            thingsInCont.remove(listForDelete.get(i).intValue());
    }

    void sortMas (int[][] array, int low, int high)
    {
        if (array[0].length == 0)
            return;
        if (low >= high)
            return;
        int middle = low + (high - low) / 2;
        int opora = array[0][middle];
        int i = low, j = high;
        while (i <= j)
        {
            while (array[0][i] < opora)
                i++;
            while (array[0][j] > opora)
                j--;
            if (i <= j)
            {
                int temp1 = array[0][i];
                array[0][i] = array[0][j];
                array[0][j] = temp1;
                int temp2 = array[1][i];
                array[1][i] = array[1][j];
                array[1][j] = temp2;
                i++;
                j--;
            }
        }
    }

    void firstFit (ArrayList<Integer> thingsToReplace, Container[] store, Entity[] things, Cell[] answer, int numOfCont, Double[][] answerWithDiv)
    {
        ArrayList<Integer> emptyCont = new ArrayList<>();
        for (int c = 0; c < numOfCont; c++)
        {
            if(answer[c].getThingsInCell().size() == 0)
                emptyCont.add(c);
        }
        for (int i = 0; i < thingsToReplace.size(); i++)
        {

            for (int c = 0; c < emptyCont.size(); c++)
            {
                boolean[] flag = {true, true};
                for (int s = 0; s < 2; s++)
                {
                    for (int t = things[thingsToReplace.get(i)].getDepartureTime(); t < things[thingsToReplace.get(i)].getLandingTime(); t++)
                    {
                        int sumWeight = 0;
                        for (int k = 0; k < answer[c].getThingsInCell().size(); k++)
                        {
                            if (checkIfThingActive(t, things[answer[c].getThingsInCell().get(k)]) && answerWithDiv[answer[c].getThingsInCell().get(k)][s] == 1)
                                sumWeight += things[answer[c].getThingsInCell().get(k)].getWeight();
                        }
                        if (sumWeight + things[thingsToReplace.get(i)].getWeight() > store[c].getDivisions().get(s))
                        {
                            flag[s] = false;
                            break;
                        }
                    }
                    if (flag[s])
                    {
                        answer[findWhereThingIs(answer, numOfCont, thingsToReplace.get(i))].getThingsInCell().remove(thingsToReplace.get(i));
                        answer[c].getThingsInCell().add(thingsToReplace.get(i));
                        answerWithDiv[thingsToReplace.get(i)][s] = 1.0;
                        break;
                    }
                }
                if (flag[0] || flag[1])
                    break;
            }
        }
    }
    //TODO: Исправить баги с FF
    void solveWithDimensions (Container[] store, Entity[] things, Cell[] answer, int numOfObj, int numOfCont)
    {
        int endTime = findFinishTime(things, numOfObj);
        int[][] answerWithDiv = new int[2][numOfObj];
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < numOfObj; j++)
                answerWithDiv[i][j] = 0;
        }
        for (int t = 0; t < endTime; t++)
        {
            for (int i = 0; i < numOfCont; i++)
            {
                int capacity = store[i].getDivisions().get(0);
                ArrayList<Integer> thingsInCont = new ArrayList<Integer>();
                for (int j = 0; j < answer[i].getThingsInCell().size(); j++)
                {
                    if (checkIfThingActive(t, things[answer[i].getThingsInCell().get(j)]))
                    {
                        if (answerWithDiv[0][answer[i].getThingsInCell().get(j)] == 0)
                            thingsInCont.add(j);
                        else
                            capacity -= things[answer[i].getThingsInCell().get(j)].getWeight();
                    }

                }
                if (thingsInCont.size() == 0)
                    continue;
                knapsack(thingsInCont, things, answerWithDiv, capacity);
                int[][] thingsInNextDiv = new int[2][thingsInCont.size()];
                for (int j = 0; j < thingsInCont.size(); j++)
                {
                    thingsInNextDiv[0][j] = things[thingsInCont.get(j)].getWeight();
                    thingsInNextDiv[1][j] = thingsInCont.get(j);
                }
                sortMas(thingsInNextDiv, 0, thingsInNextDiv[0].length - 1);
                int sumWeight = 0;
                ArrayList<Integer> listForDelete = new ArrayList<Integer>();
                for (int j = 0; j < answer[i].getThingsInCell().size(); j++)
                {
                    if (answerWithDiv[1][j] == 1 && checkIfThingActive(t, things[answer[i].getThingsInCell().get(j)]))
                        sumWeight += things[answer[i].getThingsInCell().get(j)].getWeight();
                }
                for (int j = 0; j < thingsInCont.size(); j++)
                {
                    if (sumWeight + thingsInNextDiv[0][j] <= store[i].getDivisions().get(1))
                    {
                        sumWeight += thingsInNextDiv[0][j];
                        answerWithDiv[1][thingsInNextDiv[1][j]] = 1;
                        listForDelete.add(j);
                    }
                }
                Collections.sort(listForDelete);
                for (int j = listForDelete.size() - 1; j >= 0; j--)
                    thingsInCont.remove(listForDelete.get(j).intValue());
                if (thingsInCont.size() != 0)
                {
                    //firstFit(thingsInCont, store, things, answer, numOfObj, numOfCont, t, answerWithDiv, i);
                }
            }
        }
        for (int i = 0; i < numOfObj; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (answerWithDiv[j][i] != 0 && answerWithDiv[j][i] != 1)
                {
                    System.out.println("Division is incorrect!");
                    return;
                }
            }

        }
        System.out.println("Division is correct!");
    }

    void fistFitForStart (Container[] store, Entity[] things, int numOfObj, int numOfCont, Cell[] answer)
    {
        int endTime = findFinishTime(things, numOfObj);
        boolean[] putedThings = new boolean[numOfObj];
        for (int i = 0; i < numOfObj; i++)
            putedThings[i] = false;
        for (int t = 0; t < endTime; t++)
        {
            for (int i = 0; i < numOfObj; i++)
            {
                for (int k = 0; k < numOfCont; k++)
                {
                    int sumWeight = 0;
                    if (answer[k].getThingsInCell().size() != 0)
                    {
                        for (int j = 0; j < answer[k].getThingsInCell().size(); j++)
                        {
                            if (checkIfThingActive(t, things[answer[k].getThingsInCell().get(j)]))
                                sumWeight += things[answer[k].getThingsInCell().get(j)].getWeight();
                        }
                    }
                    if (sumWeight + things[i].getWeight() <= store[k].getSumCapacity() && !putedThings[i] && checkIfThingActive(t, things[i]))
                    {
                        answer[k].getThingsInCell().add(i);
                        putedThings[i] = true;
                    }
                }
            }
        }
    }

    double lowBound (Container[] store, Entity[] things, int numOfObj)
    {
        int finishTime = findFinishTime(things, numOfObj);
        double lowBound = 0;
        for (int t = 0; t < finishTime; t++)
        {
            double sum = 0;
            for (int i = 0; i < numOfObj; i++)
            {
                if (checkIfThingActive(t, things[i]))
                    sum += things[i].getWeight();
            }
            sum /= store[0].getSumCapacity();
            if (sum > lowBound)
                lowBound = Math.ceil(sum);
        }
        return lowBound;
    }

    void simulatedAnnealing (Container[] store, Entity[] things, Cell[] answer, int numOfObj, int numOfCont, double initialTemperature, double endTemperature, PrintWriter output)
    {
        fistFitForStart(store, things, numOfObj, numOfCont, answer);
        int countForPlot = 0;
        int disToChangeSol = 0;
        int badDis = 0;
        double t = initialTemperature, fine = 0;
        double currentEnergy = calculateEnergy(fine, store, things, answer, numOfObj, numOfCont, t);
        double p;
        printForPlot(output, countForPlot, currentEnergy);
        for (int i = 0; i < 100000; i++)
        {
            Cell[] stateCandidate = new Cell[numOfCont];
            for (int j = 0; j < numOfCont; j++)
                stateCandidate[j] = new Cell();
            generateStateCandidate(answer, numOfCont, store, stateCandidate, numOfObj, things);
            double candidateEnergy = calculateEnergy(fine, store, things, stateCandidate, numOfObj, numOfCont, t);
            if (candidateEnergy <= currentEnergy)
            {
                currentEnergy = candidateEnergy;
                eqAnswer(numOfCont, answer, stateCandidate);
                disToChangeSol++;
            }
            else
            {
                p = getTransitionProbability(candidateEnergy - currentEnergy, t);
                if (makeTransit(p))
                {
                    disToChangeSol++;
                    badDis++;
                    currentEnergy = candidateEnergy;
                    eqAnswer(numOfCont, answer, stateCandidate);
                }
            }
            t -= 0.0001;//decreaseTemperature(initialTemperature, i);
            fine += 0.001;
            countForPlot++;
            printForPlot(output, countForPlot, currentEnergy);
            if (t <= endTemperature)
                break;
        }
        if (checkForCorrect(things, numOfObj, numOfCont, answer, store))
        {
            System.out.println("Solution is correct!");
            //solveWithDimensions(store, things, answer, numOfObj, numOfCont);
        }
        else
            System.out.println("Solution is incorrect!");
        System.out.println("All dicisions: ");
        System.out.println(disToChangeSol);
        System.out.println("Bad dicisions: ");
        System.out.println(badDis);
        System.out.println("Low bound: ");
        System.out.println(lowBound(store, things, numOfObj));
    }
}