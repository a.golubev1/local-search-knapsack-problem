package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Container
{
    private int numOfDivisions;
    private double sumCapacity;
    private ArrayList<Integer> divisions;

    public Container ()
    {
        divisions = new ArrayList<Integer>();
        sumCapacity = 20.0;
        numOfDivisions = 2;
        for (int i = 0; i < 2; i++)
            divisions.add(10);
    }

    public ArrayList<Integer> getDivisions(){ return divisions;}

    public int getNumOfDivisions() {return this.numOfDivisions; }

    public double getSumCapacity() {return sumCapacity;}
}