package com.company;

import java.util.Random;
import java.util.Scanner;

public class Entity
{
    private int weight;
    private int landingTime;
    private int departureTime;

    public Entity ()
    {
        final Random random = new Random();
        boolean flag1 = true, flag2 = true;
        while (flag1)
        {
            this.weight = random.nextInt(10) + 1;
            if (this.weight < 8 && this.weight > 2)
                flag1 = false;
        }
        while (flag2)
        {
            this.landingTime = random.nextInt(24);
            this.departureTime = random.nextInt(25);
            if (this.landingTime < this.departureTime)
                flag2 = false;
        }
    }

    public int getLandingTime() { return landingTime; }

    public int getDepartureTime() { return departureTime; }

    public int getWeight() { return this.weight; }
}