package com.company;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;

public class Main
{
    public static void main(String[] args) throws IOException
    {

        int numOfObj, numOfCont;
        Functions a = new Functions();
        Scanner in = new Scanner(Paths.get("c:\\Users\\alrxg\\IdeaProjects\\bachelor-degree\\input.txt"));
        numOfObj = in.nextInt();
        numOfCont = in.nextInt();
        if (numOfCont < numOfObj)
        {
            System.out.println("Solution doesn't exist");
            return;
        }
        Container[] store = new Container[numOfCont];
        for (int i = 0; i < numOfCont; i++)
            store[i] = new Container();
        Entity[] things = new Entity[numOfObj];
        for (int i = 0; i < numOfObj; i++)
            things[i] = new Entity();
        Cell[] answer = new Cell[numOfCont];
        for (int j = 0; j < numOfCont; j++)
            answer[j] = new Cell();
        String fileName = "output.txt";
        try (PrintWriter output = new PrintWriter(fileName))
        {
            a.simulatedAnnealing (store, things, answer, numOfObj, numOfCont, 10, 0.0, output);
        }
        Double[][] answerWithDiv = new Double[numOfObj][2];
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < numOfObj; j++)
                answerWithDiv[j][i] = 0.0;
        }
        Mip model = new Mip();
        model.compute(store, answer, numOfObj, numOfCont, things, answerWithDiv);
        ArrayList<Integer> thingsToReplace = new ArrayList<>();
        for (int i = 0; i < numOfObj; i++)
        {
            if (answerWithDiv[i][0] != 1 && answerWithDiv[i][1] != 1)
                thingsToReplace.add(i);
        }
        a.firstFit(thingsToReplace, store, things, answer, numOfCont, answerWithDiv);
        int bestSolution = 0;
        for (int i = 0; i < numOfCont; i++)
        {
            if (answer[i].getThingsInCell().size() != 0)
                bestSolution++;
        }
        System.out.println("Best solution: ");
        System.out.println(bestSolution);
        in.close();
    }
}
