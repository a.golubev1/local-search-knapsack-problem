package com.company;

import gurobi.*;

public class Mip
{
    void compute(Container[] store, Cell[] answer, int numOfObj, int numOfCont, Entity[] things, Double[][] answerWithDiv)
    {
        try
        {
            double capacity = 10;
            Functions a = new Functions();
            int finishTime = a.findFinishTime(things, numOfObj);
            double[] weight = new double[numOfObj];
            int[][] contArrangement = new int[numOfObj][numOfCont];
            int[][] schedule = new int[numOfObj][finishTime];
            for (int i = 0; i < numOfObj; i++)
            {
                weight[i] = things[i].getWeight();
                for(int j = 0; j < numOfCont; j++)
                    contArrangement[i][j] = 0;
            }
            for (int t = 0; t < finishTime; t++)
            {
                for (int i = 0; i < numOfObj; i++)
                {
                    if (things[i].getLandingTime() <= t && t < things[i].getDepartureTime())
                        schedule[i][t] = 1;
                    else
                        schedule[i][t] = 0;
                }
            }
            for (int i = 0; i < numOfCont; i++)
            {
                for (int c = 0; c < answer[i].getThingsInCell().size(); c++)
                    contArrangement[answer[i].getThingsInCell().get(c)][i] = 1;
            }

            GRBEnv env = new GRBEnv(true);
            env.set("logFile", "DynamicKnapsack.log");
            env.start();

            GRBModel model = new GRBModel(env);

            GRBVar[][] x = new GRBVar[numOfObj][2];
            for (int i = 0; i < numOfObj; i++)
            {
                for (int s = 0; s < 2; s++)
                {
                    String st = "x_" + String.valueOf(i) + String.valueOf(s);
                    x[i][s] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, st);

                }
            }

            GRBLinExpr expr = new GRBLinExpr();
            for (int s = 0; s < 2; s++)
            {
                for (int i = 0; i < numOfObj; i++)
                    expr.addTerm(weight[i], x[i][s]);
            }
            model.setObjective(expr, GRB.MAXIMIZE);


            for (int t = 0; t < finishTime; t++)
            {
                for (int c = 0; c < numOfCont; c++)
                {
                    for (int s = 0; s < 2; s++)
                    {
                        GRBLinExpr expr1 = new GRBLinExpr();
                        for (int i = 0; i < numOfObj; i++)
                        {
                            if (schedule[i][t] == 1 && contArrangement[i][c] == 1)
                                expr1.addTerm(weight[i], x[i][s]);
                        }
                        model.addConstr(expr1, GRB.LESS_EQUAL, capacity, "c_1");
                    }
                }
            }

            for (int i = 0; i < numOfObj; i++)
            {
                GRBLinExpr expr2 = new GRBLinExpr();
                for (int s = 0; s < 2; s++)
                {
                    expr2.addTerm(1, x[i][s]);
                }
                model.addConstr(expr2, GRB.LESS_EQUAL, 1, "c_2");
            }

            model.optimize();

            double[][] temp = new double[numOfObj][2];
            temp = model.get(GRB.DoubleAttr.X, x);
            for (int i = 0; i < numOfObj; i++)
            {
                for (int j = 0; j < 2; j++)
                    answerWithDiv[i][j] = temp[i][j];
            }

            model.dispose();
            env.dispose();

        } catch (GRBException e) {
            System.out.println("Error code: " );
        }
    }
}