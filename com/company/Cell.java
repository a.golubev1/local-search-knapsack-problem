package com.company;

import java.util.ArrayList;

public class Cell {
    private ArrayList<Integer> thingsInCell;

    public Cell()
    {
        thingsInCell = new ArrayList<Integer>();
    }

    public ArrayList<Integer> getThingsInCell() { return this.thingsInCell; }

    public int getIndexOfThing(int num) { return this.thingsInCell.get(num); }
}